import React, {useEffect, useState} from 'react';
import { API } from '../../consts/api.consts';

export function CallData (prop){

    const [element, setElement] = useState();
    
    const getElement = () =>{
       
        API.get('/'+prop.type+'/'+prop.element.name).then((res)=> { 
            setElement(res.data);
         }); 
    }

  useEffect(getElement , []); 
 return(
            <div>
           {element &&
                <div>
                    <div><p>{prop.element.name}</p></div> 
                     {element.sprites  &&
                        <div>
                        {element.sprites.front_default  &&
                            <img src={element.sprites.front_default}/>
                        }
                        

                        {element.sprites.default  &&
                        <img src={element.sprites.default}/>
                        }
                        
                        </div>
                     }
                    
                </div>
            }

            </div>
  
 )


}