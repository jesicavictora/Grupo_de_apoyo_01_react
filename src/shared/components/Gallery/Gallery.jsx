import React, {useState} from 'react';
import { CallData } from '../CallData/CallData';
import './Gallery.css'

export function Gallery(props){ 

    
         



    return(

        <div className="gallery_container">

        {props.elements.map((element, i)=>     
            <div>
                <CallData element={element} type={props.type}></CallData>
             </div>
         )
         }

        </div>
    )

}