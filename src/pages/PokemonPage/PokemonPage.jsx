import React, {useEffect, useState} from 'react';
import { API } from '../../shared/consts/api.consts';
import {Gallery} from '../../shared/components/Gallery/Gallery';


export function PokemonPage(){

    const [pokemon, setPokemon] = useState([]);
    
    const getPokemon = () =>{
       
        API.get('/pokemon').then((res)=> { 
            setPokemon(res.data.results);
         }); 
    }

  useEffect(getPokemon , []); 

    return(
       <Gallery elements={pokemon} type="pokemon"></Gallery>
    )


}