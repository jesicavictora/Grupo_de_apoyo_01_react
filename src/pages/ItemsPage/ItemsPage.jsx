import React, {useEffect, useState} from 'react';
import { API } from '../../shared/consts/api.consts';
import {Gallery} from '../../shared/components/Gallery/Gallery';


export function ItemsPage(){

    const [item, setItem] = useState([]);
    
    const getItem = () =>{
       
        API.get('/item').then((res)=> { 
          setItem(res.data.results);
         }); 
    }

  useEffect(getItem , []); 

    return(
       <Gallery elements={item} type="item"></Gallery>
    )


}