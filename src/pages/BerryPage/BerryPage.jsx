import React, {useEffect, useState} from 'react';
import { API } from '../../shared/consts/api.consts';
import {Gallery} from '../../shared/components/Gallery/Gallery';


export function BerryPage(){

    const [berry, setBerry] = useState([]);
    
    const getBerry = () =>{
       
        API.get('/berry').then((res)=> { 
          setBerry(res.data.results);
         }); 
    }

  useEffect(getBerry , []); 

    return(
       <Gallery elements={berry} type="berry"></Gallery>
    )


}