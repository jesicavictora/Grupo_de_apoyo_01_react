import React from 'react';
import { Switch, Route } from "react-router-dom";
import { BerryPage } from '../../pages/BerryPage/BerryPage';

import {HomePage} from '../../pages/HomePage/HomePage';
import { ItemsPage } from '../../pages/ItemsPage/ItemsPage';
import {PokemonPage} from '../../pages/PokemonPage/PokemonPage';



export function Routes(){


    return(
            <Switch>
                <Route path="/item">
                    <ItemsPage/>
                </Route>

               <Route path="/berry">
                    <BerryPage/>
                </Route>
                <Route path="/pokemon">
                    <PokemonPage/>
                </Route>
                <Route path="/">
                    <HomePage/>
                </Route>   
            </Switch>
          
    )

}